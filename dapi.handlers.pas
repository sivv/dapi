unit dapi.handlers;

interface

uses System.SysUtils, System.Classes, chimera.json, Web.HTTPApp,
  System.Generics.Collections;

type
  TAPIRequest = class(TObject)
  strict private
    class var FSessions : TDictionary<string, IJSONObject>;
  strict private
    FMsg: IJSONObject;
    FSession: IJSONObject;
    FRequest: TWebRequest;
    FResponse: TWebResponse;
    procedure ParseRequest;
  public
    constructor Create(ARequest : TWebRequest; AResponse : TWebResponse);
    class constructor Create;
    class destructor Destroy;

    property Msg : IJSONObject read FMsg;
    property Session : IJSONObject read FSession;
    property Request : TWebRequest read FRequest;
    procedure Send(Msg : IJSONObject); overload;
    procedure Send(Stream : TStream; const Filename, ContentType : string; Attached : boolean); overload;
    procedure SendError(Msg : string); overload;
    procedure SendError(e: Exception); overload;
    procedure Send404;
    function IsFor(const path : string) : boolean;
  end;

  TAuthHandler = reference to function(Web : TAPIRequest) : boolean;

  IAPIHandler = interface
    procedure DispatchAPI(Web : TAPIRequest; var Handled : boolean);
  end;

  TAPIHandler = class(TInterfacedObject, IAPIHandler)
  protected
    procedure DispatchAPI(Web: TAPIRequest; var Handled: Boolean);
    procedure DoDispatch(Web : TAPIRequest; var Handled : Boolean); virtual; abstract;
  end;


  TAPI = class
  strict private
    class var FHandlers : TDictionary<string, IAPIHandler>;
    class var FAuthHandler: TAuthHandler;
  public
    class constructor Create;
    class destructor Destroy;
    class property AuthHandler : TAuthHandler read FAuthHandler write FAuthHandler;
    class function Lookup(const Path : string) : IAPIHandler;
    class procedure Register(const Path : string; Handler : IAPIHandler); overload;
    class procedure Register(const Paths : TArray<string>; Handler : IAPIHandler); overload;
  end;

implementation

uses System.Hash, System.NetEncoding, System.ZLib;

{ TAPIRequest }

constructor TAPIRequest.Create(ARequest: TWebRequest; AResponse: TWebResponse);
begin
  inherited Create;
  FRequest := ARequest;
  FResponse := AResponse;
  ParseRequest;
end;

class constructor TAPIRequest.Create;
begin
  FSessions := TDictionary<string, IJSONObject>.Create;
end;

class destructor TAPIRequest.Destroy;
begin
  FSessions.Free;
end;

function TAPIRequest.IsFor(const path: string): boolean;
begin
  result := (Length(Path) = Length(FRequest.PathInfo)) and (String.CompareText(Path, FRequest.PathInfo) = 0);
end;

procedure TAPIRequest.ParseRequest;
var
  sMsg, cookie: string;
begin
  sMsg := string(Request.ReadString(Request.ContentLength));
  try
    FMsg := JSON(sMsg);
  except
    FMsg := JSON(TNetEncoding.URL.Decode(sMsg));
  end;

  cookie := TNetEncoding.url.decode(Request.CookieFields.Values['_api_session']);

  if cookie <> '' then
  begin
    TMonitor.Enter(FSessions);
    try
      if not FSessions.TryGetValue(cookie, FSession) then
      begin
        FSession := JSON();
        FSessions.Add(cookie, FSession);
      end;
    finally
      TMonitor.Exit(FSessions);
    end;
  end else
  begin
    FSession := JSON();
    var g : TGuid;
    CreateGuid(g);
    var sToken := DateTimeToStr(now)+GuidToString(g);
    cookie := THashSHA1.GetHashString(sToken);
    FSessions.Add(cookie, FSession);
    var c := FResponse.Cookies.Add;
    c.Name := '_api_session';
    c.Value := cookie;
    c.Expires := IncMonth(Now,1);
  end;

end;

procedure TAPIRequest.Send(Msg: IJSONObject);
var
  bs : TBytesStream;
begin
  FResponse.ContentType := 'application/json';
  FResponse.StatusCode := 200;
  //FResponse.ContentEncoding := 'gzip';
  bs := TBytesStream.Create(TEncoding.UTF8.GetBytes(Msg.AsJSON));
  try
    FResponse.ContentLength := bs.Size;
    FResponse.ContentStream := bs;
  finally
    // bs.Free; // DO NOT FREE.  Will be freed by Response Object;
  end;
end;

procedure TAPIRequest.Send(Stream: TStream; const Filename, ContentType : string; Attached : boolean);
begin
  Stream.Position := 0; // Go to the start of the stream
  FResponse.ContentStream := Stream;
  FResponse.ContentType := ContentType;
  if Attached then
    if FIlename <> '' then
      FResponse.CustomHeaders.Add('Content-Disposition: attachment; filename="'+Filename+'"')
    else
      FResponse.CustomHeaders.Add('Content-Disposition: attachment');

  FResponse.SendResponse;
end;

procedure TAPIRequest.Send404;
begin
  FResponse.ContentType := 'text/plain';
  FResponse.Content := 'Not Found '+#13#10#13#10+Request.Content;
  FResponse.StatusCode := 404;
end;

procedure TAPIRequest.SendError(e: Exception);
begin
  FResponse.ContentType := 'text/plain';
  FResponse.StatusCode := 500;
  FResponse.Content := e.Message + ' @STACKTRACE: ' + e.StackTrace;
  FResponse.ReasonString := e.Message;
end;

procedure TAPIRequest.SendError(Msg: string);
begin
  FResponse.ContentType := 'text/plain';
  FResponse.StatusCode := 500;
  FResponse.Content := Msg;
  FResponse.ReasonString := Msg;
end;

{ TAPI }

class constructor TAPI.Create;
begin
  FHandlers := TDictionary<string, IAPIHandler>.Create;
end;

class destructor TAPI.Destroy;
begin
  FHandlers.Free;
end;

class function TAPI.Lookup(const Path: string): IAPIHandler;
begin
  if not FHandlers.TryGetValue(Path, Result) then
    Result := nil;
end;

class procedure TAPI.Register(const Path: string; Handler: IAPIHandler);
begin
  FHandlers.AddOrSetValue(Path, Handler);
end;

class procedure TAPI.Register(const Paths: TArray<string>;
  Handler: IAPIHandler);
begin
  for var s in Paths do
  begin
    FHandlers.AddOrSetValue(s, Handler);
  end;
end;

{ TAPIHandler }

procedure TAPIHandler.DispatchAPI(Web: TAPIRequest; var Handled: Boolean);
begin
  try
    DoDispatch(Web, Handled);
  except
    on E : Exception do
      Web.SendError(E);
  end;
end;

end.
