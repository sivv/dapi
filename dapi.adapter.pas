unit dapi.adapter;

interface

uses System.SysUtils, System.Classes, Web.HTTPApp, System.Generics.Collections,
  dapi.handlers;

type
  TAuthenticateEvent = procedure(Sender : TObject; Request : TWebRequest; var Authenticated : boolean) of object;
  TAPIAdapter = class(TComponent)
  private
    FOldDispatch : THTTPMethodEvent;
    FWebModule: TWebModule;
    FOnAuthenticate: TAuthenticateEvent;
    procedure SetWebModule(const Value: TWebModule);
    procedure APIDispatch(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure SetOnAuthenticate(const Value: TAuthenticateEvent);
  protected
    procedure Loaded; override;
    procedure UnhookDispatch; virtual;
    procedure HookDispatch; virtual;
  published
    property WebModule : TWebModule read FWebModule write SetWebModule;
    property OnAuthenticate : TAuthenticateEvent read FOnAuthenticate write SetOnAuthenticate;
  end;

implementation

{ TAPIAdapter }

procedure TAPIAdapter.APIDispatch(Sender: TObject; Request: TWebRequest;
    Response: TWebResponse; var Handled: Boolean);
begin
  Handled := False;
  var ai := TAPI.Lookup(Request.PathInfo);
  if Assigned(ai) then
  begin
    var web := TAPIRequest.Create(Request, Response);
    ai.DispatchAPI(web, Handled);
  end;
end;

procedure TAPIAdapter.HookDispatch;
begin
  if Assigned(FWebModule) then
  begin
    FOldDispatch := FWebModule.BeforeDispatch;
    FWebModule.BeforeDispatch := APIDispatch;
  end;
end;

procedure TAPIAdapter.Loaded;
begin
  inherited;
  HookDispatch;
end;

procedure TAPIAdapter.SetOnAuthenticate(const Value: TAuthenticateEvent);
begin
  FOnAuthenticate := Value;
  if assigned(FOnAuthenticate) then
  begin
    TAPI.AuthHandler :=
      function(Web : TAPIRequest) : boolean
      begin
        Result := False;
        FOnAuthenticate(Self, Web.Request, Result);
      end;
  end else
    TAPI.AuthHandler := nil;
end;

procedure TAPIAdapter.SetWebModule(const Value: TWebModule);
begin
  if Assigned(FWebModule) then
  begin
    FWebModule.RemoveFreeNotification(Self);
    UnhookDispatch;
  end;
  FWebModule := Value;
  if Assigned(FWebModule) then
  begin
    FWebModule.FreeNotification(Self);
    HookDispatch;
  end;
end;

procedure TAPIAdapter.UnhookDispatch;
begin
  if Assigned(FWebModule) then
  begin
    FWebModule.BeforeDispatch := FOldDispatch;
    FOldDispatch := nil;
  end;
end;

end.
